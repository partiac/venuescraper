#! /usr/bin/python

import requests, validators, os, MySQLdb, re, json, codecs, webbrowser, time, signal, sys, datetime
from pprint import pprint
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.proxy import *
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

db_creds = {
    "server" : "partiac.com",
    "user" : "scraper",
    "passwd" : "p-scraper-123",
    "db" : "partiac"
}
db = MySQLdb.connect(
                        db_creds["server"],
                        db_creds["user"],
                        db_creds["passwd"],
                        db_creds["db"])
cookies = {"c_user": "1269714286", "xs": "2%3A9afQQKA2HfXxdg%3A2%3A1517282364%3A5128%3A2285"}
outFile = "logs/{}.log".format(datetime.datetime.now())
def getChromeDriver():
    chrome_options = Options()
    chrome_options.add_experimental_option("prefs", {'profile.managed_default_content_settings.images': 2})
    driverPath = "./chromedriver-mac"
    driver = webdriver.Chrome(chrome_options = chrome_options, executable_path=driverPath)
    return driver

def getSearchResults(searchText):
    s = requests.Session()
    url = 'https://www.facebook.com/search/pages/?q={}'.format(searchText)
    r = s.get(url, cookies=cookies)
    fileName = "/tmp/fb_search.html"
    file = codecs.open(fileName, mode='w', encoding='utf-8')
    file.write(r.text)
    file.close()


    browser.get('file://'+fileName)
    html = browser.page_source


    soup = BeautifulSoup(html, "html.parser")
    return soup

def getId(soup):
    for tag in soup.find_all(id="all_search_results"):
        for tag1 in soup.find_all(class_="_3u1 _gli"):
            data = json.loads(tag1.get("data-bt"))
            return data["id"]
    return None

def getVenuesNeedFBId():
    sql = "SELECT id, phone, title FROM users WHERE role_id = 2 AND phone <> '' AND facebook_url = '' ORDER BY created DESC"
    cursor = db.cursor()
    try:
        cursor.execute(sql)
    except Exception as e:
        print(e)

    venuesResult = cursor.fetchall()
    return venuesResult


def writeToFile(name, fbUrl):
    saveFile = codecs.open(outFile, mode='a', encoding='utf-8')
    saveFile.write("{}, {}\n".format(name, fbUrl))
    saveFile.close()

def saveVenue(id, fbUrl, name):
    sql = "UPDATE users SET facebook_url = '{}' WHERE id = {}".format(fbUrl, id)
    cursor = db.cursor()
    try:
        cursor.execute(sql)
    except Exception as e:
        print(e)
        db.rollback()
        return
    db.commit()
    writeToFile(name, fbUrl)

def getFullUrl(fbUrl):
    browser.get(fbUrl)
    fullUrl = browser.current_url
    print fullUrl
    return fullUrl


def scrapeVenue(id, phone, name):
    phone = phone.replace(' ', '')
    soup = getSearchResults(phone)
    fbId = getId(soup)
    if fbId is not None:
        fbUrl = "http://facebook.com/{}".format(fbId)
        fbUrl = getFullUrl(fbUrl)
        saveVenue(id, fbUrl, name)
    else:
        print "No URL for: " + name

def setFBCookies():
    global cookies
    cookieFile = codecs.open('cookies.json')
    cookies = json.loads(cookieFile.read())
    cookieFile.close()
    browser.get("http://facebook.com")
    browser.delete_all_cookies()
    for name, value in cookies.iteritems():
        browser.add_cookie({'name': name, 'value': value})

    while True:
        browser.get("http://facebook.com")
        choice = raw_input("Make sure facebook is logged in, type 'd' if it is, type 'r' if it isn't: ")
        if choice == 'r':
            browser.delete_all_cookies()
            browser.get("http://facebook.com")
            raw_input("Login to facebook and hit Enter when you are done.")
        elif choice == 'd':
            break


    c_user = browser.get_cookie('c_user')
    xs = browser.get_cookie('xs')
    cookies = {'c_user': c_user['value'], 'xs': xs['value']}
    cookieJSON = json.dumps(cookies)
    cookieFile = codecs.open('cookies.json', 'w')
    cookieFile.write(cookieJSON)
    cookieFile.close()

def sigint_handler(signum, frame):
    browser.quit()
    sys.exit()
signal.signal(signal.SIGINT, sigint_handler)

if __name__ == "__main__":
    browser = getChromeDriver()
    setFBCookies()
    venues = getVenuesNeedFBId()
    for venue in venues:
        scrapeVenue(venue[0], venue[1], venue[2])
    browser.quit()
