#! /usr/bin/python
import requests, validators, os, sys, MySQLdb, re, json, codecs, unicodedata, webbrowser, csv

import Tkinter as tk, tkFileDialog as filedialog
from pprint import pprint
from bs4 import BeautifulSoup
from terminaltables import AsciiTable
db_creds = {
    "server" : "partiac.com",
    "user" : "scraper",
    "passwd" : "p-scraper-123",
    "db" : "partiac"
}
db = MySQLdb.connect(
                        db_creds["server"],
                        db_creds["user"],
                        db_creds["passwd"],
                        db_creds["db"])

ignoredHandles = [
                    "yelp",
                    "_u",
                    "share",
                    "p",
                    "widgets.js",
                    "sitebuilderhelp",
                    "twitter",
                    "instagram",
                    "yp",
                    "oct.js",
                    "vp",
                    "thrillist",
                    "intent",
                    "i"

]
class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   STRIKETHROUGH = '\033[9m'
   END = '\033[0m'

def bold(val):
    return color.BOLD + "{}".format(val) + color.END

def strikethrough(val):
    return color.RED + "{}".format(val) + color.END

def pickLinks(linkDic, title):
    finalLinkDic = {}
    for site, links in linkDic.iteritems():
        if len(links) > 0:
            if len(links) == 1:
                finalLinkDic[site] = links[0]
                continue
            tableData = [["#", "handle for {}".format(bold(title))]]
            for i in range(len(links)):
                url = "http://{}.com/{}".format(site, links[i])
                tableData.append([i, links[i], url])
            choice = None
            while choice is None:
                table = AsciiTable(tableData)
                print "\n"
                print table.table
                choice = raw_input("Select {} link for {} or -1 for no link: ".format(bold(site), bold(title)))

                try:
                    choice = int(choice)
                except ValueError:
                    print "Invalid Choice"
                    choice = None
                    continue
                if choice < -1 or choice >= len(links):
                    print "Invalid Choice"
                    choice = None
            if choice >= 0:
                finalLinkDic[site] = links[choice]

    return finalLinkDic



def getSocialLinks(url, title, needsDic):
    if url[:4] != "http":
        url = "http://" + url
    linkDic = {"instagram": [], "twitter": []}
    session = requests.Session()
    try:
        page = session.get(url, timeout=2.0)
    except Exception as e:
        print(e)
        return {}
    soup = BeautifulSoup(page.text, 'html.parser')
    html = soup.prettify()

    links = re.findall("(twitter|instagram).com/([a-zA-Z0-9-_\.]*)/?", html)
    for linkTup in links:
        if linkTup[1] != 'explore' and linkTup[1] != '' and linkTup[1].lower() not in ignoredHandles:
            site = linkTup[0]
            handle = linkTup[1]
            if site == 'twitter':
                handle = "@" + handle
            if handle not in linkDic[site] and needsDic[site]:
                linkDic[site].append(handle)
            # if site not in linkDic:
            #     linkDic[site] = handle
    return pickLinks(linkDic, title)

def showAddedLinks(addedLinks):
    tableData = [["title", "twitter", "instagram"]]
    for title, links in addedLinks.iteritems():
        tableData.append([title, links.get("twitter", ""), links.get("instagram", "")])
    table = AsciiTable(tableData)
    print "\n"
    print table.table


def csvMain():
    root = tk.Tk()
    root.withdraw()
    root.update()
    file_path = filedialog.askopenfilename(initialdir=os.getcwd() + "/venues_csv")
    tmp_file_path = file_path + ".tmp"
    fields = ["title", "address", "phone", "website_url", "hours_string"]
    outFields = fields + ["twitter", "instagram"]
    needsDic = {"twitter": True,
                "instagram": True}
    with open(file_path) as csvfile:
        reader = csv.DictReader(csvfile, fieldnames=fields)
        for row in reader:
            title = row['title']
            url = row['website_url']
            if url is not None and url != "":
                socialLinks = getSocialLinks(url, title, needsDic)
                row.update(socialLinks)
            with open(tmp_file_path, mode='a') as tmpCsvfile:
                writer = csv.DictWriter(tmpCsvfile, fieldnames=outFields)
                writer.writerow(row)
    os.rename(tmp_file_path, file_path)

if __name__ == "__main__":
    ignoredFile = open('ignoredHandles.json')
    ignoredString = ignoredFile.read()
    ignoredFile.close()
    details = json.loads(ignoredString)
    ignoredHandles = details["ignoredHandles"]

    if raw_input("Type 'o' to select csv file to use, enter to skip: ") or None:
        csvMain()
        sys.exit()
    skipFile = open('skip_website.json')
    skipString = skipFile.read()
    skipFile.close()
    skip = json.loads(skipString)["skip"]

    cursor = db.cursor()
    sql = "SELECT id, website_url, twitter_url, instagram_url, title from users WHERE role_id = 2 AND website_url <> '' AND (twitter_url = '' OR instagram_url = '')"
    try:
        cursor.execute(sql)
    except Exception as e:
        print(e)

    addedLinks = {}
    venues = cursor.fetchall()
    count = 0
    for venueResult in venues:
        count = count + 1

        venue = venueResult[0]
        url = venueResult[1]
        if venue in skip:
            print strikethrough("{}/{} {} SKIPPED".format(count, len(venues), url))
            continue
        print "{}/{} {}".format(count, len(venues), url)
        needsTwitter = venueResult[2] == ''
        needsInstagram = venueResult[3] == ''
        needsDic = {"twitter": needsTwitter,
                    "instagram": needsInstagram}
        title = venueResult[4]
        socialLinks = getSocialLinks(url, title, needsDic)
        if len(socialLinks) > 0:
            addedLinks[title] = socialLinks
            setStrings = []
            for site, handle in socialLinks.iteritems():
                if (site == 'twitter' and needsTwitter) or (site == 'instagram' and needsInstagram):
                    setStrings.append("{}_url = '{}'".format(site, handle))
            setString = ", ".join(setStrings)
            if setString != '':
                sql = "UPDATE users SET {} WHERE id = {}".format(setString, venue)
                print sql
                try:
                    cursor.execute(sql)
                except Exception as e:
                    print(e)
                    continue
                db.commit()
        skip.append(venue)
        skipFile = open('skip_website.json', mode='w')
        skipJson = json.dumps({"skip": skip})
        skipFile.write(skipJson)
        skipFile.close()
    showAddedLinks(addedLinks)
