import csv
import MySQLdb
class OutputHandler :
    db_creds = {
        "server" : "partiac.com",
        "user" : "scraper",
        "passwd" : "p-scraper-123",
        "db" : "partiac"
    }
    def __init__(self, filename):
        self.db = MySQLdb.connect(
                                            self.db_creds["server"],
                                            self.db_creds["user"],
                                            self.db_creds["passwd"],
                                            self.db_creds["db"])
        if filename is not None:
            self.filename = filename
            self.file = open('venues_csv/{}.csv'.format(filename), 'a')
            self.csvWriter = csv.writer(self.file)

    def writeVenueToFile(self, venue):
        title = venue["title"]
        phone = venue.get("phone", "")
        address = venue.get("address", "")
        # city = venue.get("city", "")
        state = venue.get("state", "")
        combinedAddress = "{}, {}".format(address, state)
        website_url = venue.get("website_url", "")
        hours_string = ""
        if "opening_hours" in venue["place"].details:
            unparsedHours = venue["place"].details["opening_hours"]
            hours_string = self.parseHoursString(unparsedHours)

        row = [title, address, phone, website_url, hours_string]
        self.csvWriter.writerow(row)


    def parseHoursString(self, hours_of_operation):
        day_names = {0: "Sun", 1: "Mon", 2: "Tue", 3: "Wed", 4: "Thu", 5: "Fri", 6: "Sat"}
        hours_dic = {}
        inc_days = []
        if hours_of_operation is not None:
            periods = hours_of_operation["periods"]
            for dayHours in periods:
                day = dayHours["open"]["day"]
                openHour = int(dayHours["open"]["time"][:2])
                openMin = dayHours["open"]["time"][2:]
                openSym = "am"
                if openHour > 12:
                    openHour = openHour % 12
                    openSym = "pm"
                openTime = "{}:{} {}".format(openHour, openMin, openSym)
                closeTime = "?"
                if "close" in dayHours:
                    closeHour = int(dayHours["close"]["time"][:2])
                    closeMin = dayHours["close"]["time"][2:]
                    closeSym = "am"
                    if closeHour > 12:
                        closeHour = closeHour % 12
                        closeSym = "pm"
                    closeTime = "{}:{} {}".format(closeHour, closeMin, closeSym)
                openString = "{} - {}".format(openTime, closeTime)
                hours_dic[day] = openString
        days_string = ""
        for day, hour_string in hours_dic.iteritems():
            if day not in inc_days:
                nextDay = day + 1
                inc_days.append(day)
                has_consecutive = False
                while hours_dic.get(nextDay) == hour_string:
                    has_consecutive = True
                    inc_days.append(nextDay)
                    nextDay = nextDay + 1
                if has_consecutive:
                    day_string = "{} - {}: {} ".format(day_names[day], day_names[inc_days[-1]], hour_string)
                else:
                    day_string = "{}: {} ".format(day_names[day], hour_string)
                days_string = days_string + day_string

        return days_string

    def saveHoursOfOperation(self, venue):
        idSQL = "SELECT LAST_INSERT_ID()"
        cursor = self.db.cursor()
        try:
            cursor.execute(idSQL)
        except Exception as e:
            print(e)
            return

        venueInfo = cursor.fetchall()
        venueId = venueInfo[0][0]

        hours_of_operation = venue["hours_of_operation"]
        for day in hours_of_operation:
            sql = "INSERT INTO hours_of_operation (user_id, day, is_open, start_time, end_time, is_next_day, created, updated) VALUES ({}, {}, 1, \"{}\", \"{}\", {}, NOW(), NOW())".format(venueId, day["day"], day["start_time"], day["end_time"], day["is_next_day"])

            try:
                cursor.execute(sql)
                self.db.commit()
            except Exception as e:
                print(e)
                self.db.rollback()


    def checkIfPossibleDuplicate(self, venue):
        if self.filename is not None:
            return None
        splitAddr = venue["address"].split(',')[0].split(' ')
        try:
            street = splitAddr[1]
            if len(street) <= 2:
                street = splitAddr[2]
            checkAddr = "{} %{}".format(splitAddr[0], street)
            checkNAddr = "{} N {}".format(splitAddr[0], street)
            checkEAddr = "{} E {}".format(splitAddr[0], street)
            checkSAddr = "{} S {}".format(splitAddr[0], street)
            checkWAddr = "{} W {}".format(splitAddr[0], street)
        except Exception as e:
            venueDetails = {
                                "title": "Error",
                                "address": "Error"
            }
            return venueDetails


        print checkAddr
        sql = "SELECT title, address FROM users WHERE address LIKE \"{}%\"".format(checkAddr)
        # sql = "SELECT title, address FROM users WHERE address LIKE \"%{}%\" OR address LIKE \"%{}%\" OR address LIKE \"%{}%\" OR address LIKE \"%{}%\" OR address LIKE \"%{}%\" ".format(checkAddr, checkNAddr, checkEAddr, checkSAddr, checkWAddr)

        cursor = self.db.cursor()
        try:
            cursor.execute(sql)
        except Exception as e:
            print(e)
            return

        venueInfo = cursor.fetchall()
        if len(venueInfo) > 0:
            venueDetails = {
                                "title": venueInfo[0][0],
                                "address": venueInfo[0][1]
            }
            return venueDetails
        else:
            return None

    def uploadVenueToServer(self, venue):
        if self.filename is not None:
            self.writeVenueToFile(venue)
            return
        title = venue["title"]
        phone = venue.get("phone", "")
        lat = venue.get("lat", 0)
        lon = venue.get("lon", 0)
        image = venue.get("image", "")
        address = venue.get("address", "")
        city = venue.get("city", "")
        state = venue.get("state", "")
        website_url = venue.get("website_url", "")

        sql = "INSERT INTO users (role_id, title, phone, lat, lon, address_lat, address_lon, image, address, city, state, website_url, status) VALUES (2, \"{}\", \"{}\", {}, {}, {}, {}, \"{}\", \"{}\", \"{}\", \"{}\", \"{}\", 0)".format(title, phone, lat, lon, lat, lon, image, address, city, state, website_url)

        cursor = self.db.cursor()
        try:
            # Execute the SQL command
            cursor.execute(sql)
            # Commit your changes in the database
            self.db.commit()
        except Exception as e:
            print(e)
            print(sql)
            self.db.rollback()
            return

        self.saveHoursOfOperation(venue)
