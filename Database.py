import MySQLdb
from pymongo import MongoClient
import datetime
class PartiacDB:
    db_creds = {
        "server" : "partiac.com",
        "user" : "scraper",
        "passwd" : "p-scraper-123",
        "db" : "partiac"
    }

    mongo_creds = {
        "server" : "hours.partiac.com",
        "port" : 27017,
        "user" : "mainHoursUser",
        "passwd" : "partiacMainHours",
        "db" : "popular_hours"
    }

    def __init__(self):
        self.db = MySQLdb.connect(
                                            self.db_creds["server"],
                                            self.db_creds["user"],
                                            self.db_creds["passwd"],
                                            self.db_creds["db"])
        mongo_creds = self.mongo_creds
        mongoString = "mongodb://{}:{}@{}:{}/{}".format(mongo_creds["user"], mongo_creds["passwd"], mongo_creds["server"], mongo_creds["port"], mongo_creds["db"])
        self.mongoClient = MongoClient(mongoString)
        self.mongoCollection = self.mongoClient.popular_hours.main_hours



    def getVenues(self):
        venues = []
        sql = "SELECT id, title, google_places_id, lat, lon, address, timezone from users WHERE role_id = 2 AND status = 1 AND skip_scrape = 0 AND google_places_id IS NOT NULL AND google_places_id <>'';"
        cursor = self.db.cursor()
        cursor.execute(sql)
        data = cursor.fetchall()
        if data is not None:
            for venueData in data:
                venue = {}
                venue["id"] = venueData[0]
                venue["name"] = venueData[1]
                venue["gid"] = venueData[2]
                venue["lat"] = venueData[3]
                venue["lon"] = venueData[4]
                venue["address"] = venueData[5]
                venue["timezone"] = venueData[6]
                venues.append(venue)
        return venues

    def getVenueHOA(self, venue, day):
        day = day + 1
        sql = "SELECT start_time, end_time FROM hours_of_operation WHERE user_id = %(venueId)s AND day = %(day)s;"
        cursor = self.db.cursor()
        cursor.execute(sql, {'venueId':venue.id, 'day': day})
        data = cursor.fetchone()
        if data is None:
            return None
        else:
            formattedData = {
                "open": data[0],
                "close": data[1]
            }
            return formattedData
    def getHOAForVenues(self, venues, day):
        day = day + 1
        placeholders= ', '.join(['%s']*len(venues))  # "%s, %s, %s, ... %s"

        sql = "SELECT user_id, start_time, end_time FROM hours_of_operation WHERE user_id IN ({}) AND day = %s;".format(placeholders)

        cursor = self.db.cursor()
        parameters = tuple(venues) + (day,)
        cursor.execute(sql, parameters)
        data = cursor.fetchall()
        venuesHOA = {}
        if data is None:
            return None
        else:
            for venueData in data:
                venueId = venueData[0]
                formattedData = {
                    "open": venueData[1],
                    "close": venueData[2]
                }
                venuesHOA[venueId] = formattedData
            return venuesHOA


    def getGoogleIDFor(self, id):
        sql = "SELECT title, google_places_id, lat, lon, address, timezone FROM users WHERE id = %(id)s;"

        cursor = self.db.cursor()
        cursor.execute(sql, {'id':id})
        data = cursor.fetchone()
        if data is not None:
            formattedData = {
                "name" : data[0],
                "gid" : data[1],
                "lat" : data[2],
                "lon" : data[3],
                "address" : data[4],
                "timezone" : data[5]
            }
            return formattedData
        else:
            print sql


    def saveGid(self, id, gid):
        sql = "UPDATE users set google_places_id = %(gid)s WHERE id = %(id)s;"

        cursor = self.db.cursor()
        try:
           # Execute the SQL command
           cursor.execute(sql, {'id':id, 'gid': gid})
           # Commit your changes in the database
           self.db.commit()
        except:
           # Rollback in case there is any error
           self.db.rollback()

    def saveHours(self, venue):

        if venue.hours is not None:

            result = self.mongoCollection.update_one({'venue': venue.id}, {'$set': {'name': venue.name, 'popular': venue.hours, 'updated' : datetime.datetime.utcnow()}}, upsert=True)


        else:
            print "No Hours"

    def getDaysHours(self, day, venue):
        field = "popular.{}".format(day)
        result = self.mongoCollection.find_one({'venue': venue.id}, {field: 1})
        if result:
            popular = result["popular"]
            if day in popular:
                dayHours = result["popular"][day]
                return dayHours
            else:
                return None
        else:
            return None

    def setTodaysCrowdForVenue(self, hours, venue):
        for key in hours.keys():
            if key < 10 or key > 28:
                hours.pop(key)
        keys = hours.keys()
        vals = hours.values()

        keys.insert(0, "id")
        vals.insert(0, venue)

        keyString = ','.join("`{}`".format(str(e)) for e in keys)
        valString = ','.join(str(e) for e in vals)

        keyVals = []
        for key, val in hours.items():
            keyVals.append("`{}` = {}".format(key, val))
        keyValString = ', '.join(keyVals)

        sql = "INSERT INTO todays_hours ({}) VALUES ({}) ON DUPLICATE KEY UPDATE {};".format(keyString, valString, keyValString)

        cursor = self.db.cursor()
        try:
            # Execute the SQL command
            cursor.execute(sql)
            # Commit your changes in the database
            self.db.commit()
        except Exception as e:
            print(e)
            print(sql)
            print(hours)
            print "Error saving current crowd!"
            # Rollback in case there is any error
            self.db.rollback()
    def setCrowdForVenue(self, crowd, venue):
        sql = "UPDATE users set current_crowd = %(crowd)s WHERE id = %(id)s;"

        cursor = self.db.cursor()
        try:
            # Execute the SQL command
            cursor.execute(sql, {'id':venue, 'crowd': crowd})
            # Commit your changes in the database
            self.db.commit()
        except:
            print "Error saving current crowd!"
            # Rollback in case there is any error
            self.db.rollback()

    def getVenuesNeedGID(self):
        # sql = "SELECT id, title, lat, lon FROM users WHERE role_id = 2 AND google_places_id IS NULL AND status = 1;"
        sql = "SELECT id, title, lat, lon FROM users WHERE role_id = 2 AND google_places_id IS NULL;"
        cursor = self.db.cursor()
        cursor.execute(sql)
        data = cursor.fetchall()
        venues = []
        if data is not None:
            for venue in data:
                venueData = {
                    "id" : venue[0],
                    "name" : venue[1],
                    "lat" : venue[2],
                    "lon" : venue[3]
                }
                venues.append(venueData)
        return venues

    def close(self):
        self.db.close()
        self.mongoClient.close()
