#! /usr/bin/python
import threading
import urllib, json, requests, subprocess, sys, re, unicodedata
from Database import PartiacDB
def getGID(name, lat, lon):


    url = "https://www.google.com/maps/place/{}/@{},{},17z/".format(name, lat, lon)
    print url
    proxies = {
                "http" : "http://108.59.14.208:13040",
                "https" : "http://108.59.14.203:13040"
                }
    # response = requests.get(url, proxies=proxies)
    response = requests.get(url)
    html = response.text
    # search = re.match('0x[0-9a-z]\{16\}:0x[0-9a-z]\{16\}', html)

    myre = re.compile('0x[0-9a-z]{16}:0x[0-9a-z]{16}', re.UNICODE)

    search = myre.search(html)
    gid = None
    normalGid = None
    if search:
        gid = search.group(0)
    if gid:
        normalGid = unicodedata.normalize('NFKD', gid).encode('ascii','ignore')

    return normalGid

class GIDGrabber(threading.Thread):

    def __init__(self, pool):
        threading.Thread.__init__(self)
        self.pool = pool
        self.db = PartiacDB()

    def run(self):
        venue = self.pool.next()
        while venue:
            gid = getGID(venue["name"], venue["lat"], venue["lon"])
            id = venue["id"]
            print id, gid
            self.db.saveGid(id, gid)
            venue = self.pool.next()
        self.db.close()

class Pool:
    def __init__(self):
        db = PartiacDB()
        self.venues = db.getVenuesNeedGID()
        db.close()
        print len(self.venues)
    def next(self):
        if self.venues:
            return self.venues.pop()
        else:
            return None

if __name__ == "__main__":
    numThreads = 10

    pool = Pool()
    for x in range(numThreads):

        grabber = GIDGrabber(pool)
        grabber.start()
