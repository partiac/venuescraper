#! /usr/bin/python
# from __future__ import print_function
from googleplaces import GooglePlaces, types, lang, GooglePlacesError
from OutputHandler import OutputHandler
import time
import math
import pyperclip
import webbrowser
from terminaltables import AsciiTable
import pprint
import codecs
import os, sys, json, fnmatch

API_KEY = "AIzaSyCEk3QQvPeMxAO1zTSOK4Jl59Ny_gjsikI"
#API_KEY = "AIzaSyBBlkrmZfYfof5DNaKX3RsLgnO_EUHNBUk"
google_places = GooglePlaces(API_KEY)
venues = []
places = []
removedPlaces = []

# mandatoryTypes = ["bar", "night_club"]
# acceptedTypes = ["bar", "night_club", "restaurant", "establishment", "food", "point_of_interest"]

class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'



def milesToMeters(miles):
    return max(miles * 1609.34, 50000)

def scrape(lat, lon, radius, venue_type, sleepTime):
    radius = milesToMeters(radius)
    pt = []
    first = True
    while pt != [] or first is True:
        try:
            results = startIndividualScrape(lat, lon, radius, venue_type, pt)
        except Exception as e:
            print(e)
            return False
        else:
            first = False
            pt = results.next_page_token
            handleResults(results)
            time.sleep(sleepTime)

    return True

def showTableForVenues():
    tableData = [["num", "Name","Has Images", "Chosen Image"]]

    for i in range(len(venues)):
        venue = venues[i]
        # types = ', '.join(str(x) for x in place.types)
        currentImage = ""
        if "image" in venue:
            currentImage = venue["image"]
        data = [i, venue["title"], len(venue["place"].photos) > 0, currentImage]
        tableData.append(data)
    table = AsciiTable(tableData)
    print("\n")
    print(table.table)

def showTableForRemoved():
    tableData = [["num", "Name", "Categories"]]

    for i in range(len(removedPlaces)):
        place = removedPlaces[i]
        types = ', '.join(str(x) for x in place.types)
        # data = [i, place.name, place.vicinity]
        types = ', '.join(str(x) for x in place.types)
        data = [i, place.name, types]
        tableData.append(data)
    table = AsciiTable(tableData)
    print("\n")
    print(table.table)


def showTableForPlaces():
    tableData = [["num", "Name", "Address"]]

    for i in range(len(places)):
        place = places[i]
        types = ', '.join(str(x) for x in place.types)
        data = [i, place.name, place.vicinity]
        # data = [i, place.name, types]
        tableData.append(data)
    table = AsciiTable(tableData)
    print("\n")
    print(table.table)

def nameIsInvalid(name):
    # blacklist = [
    #                 "BBQ",
    #                 "Steakhouse",
    #                 "Bistro",
    #                 "Cafe",
    #                 "Gentlemans Club",
    #                 "Strip Club",
    #                 "Wine",
    #                 "Tapas",
    #                 "Tacos",
    #                 "Seafood",
    #                 "Fish",
    #                 "Chicken"
    # ]

    return any(forbidden in name for forbidden in blacklist)
def removeUnwanted():
    global places, removedPlaces
    newPlaces = []

    for place in places:
        if len(set(place.types) - set(acceptedTypes)) == 0 and place.types[0] in mandatoryTypes and not nameIsInvalid(place.name):
            newPlaces.append(place)
        else:
            removedPlaces.append(place)

    print("{} removed".format(len(places) - len(newPlaces)))
    places = newPlaces

def removeDuplicatePlaces():
    newPlaces = []
    global places
    ids = []
    for i in range(len(places)):
        place = places[i]
        if place.place_id not in ids and place.place_id not in skip:
            ids.append(place.place_id)
            newPlaces.append(place)
    places = newPlaces

def addImageToVenueAt(index):
    global venues
    venue = venues[index]
    done = False
    while not done:
        numImages = len(venue["place"].photos)
        if numImages == 0:
            print("{} has no images".format(venue["title"]))
            done = True
            continue
        print("{} has {} images".format(venue["title"], numImages))
        option = raw_input("p to preview an image, s to set an image, d to return: ")
        if option == "p" or option == "s":
            imageSelection = raw_input("select an image between 0 and {}: ".format(numImages - 1))
            try:
                imageSelection = int(imageSelection)
            except ValueError:
                imageSelection = -1
            if imageSelection not in range(numImages):
                print("Invalid Image")
                continue
            photo = venue["place"].photos[imageSelection]
            photo.get(maxheight=2000)
            if option == "p":
                webbrowser.open_new_tab(photo.url)
            elif option == "s":
                venues[index]["image"] = photo.url
                done = True

            continue

        elif option == "d":
            done = True
            continue
        else:
            print("Invalid Option")
            continue

def addVenueImages():

    rootDir = os.getcwd()
    print "\n"
    for venue in venues:
        sys.stdout.write("\033[K")
        print "Saving images for {}...\r".format(venue["title"]),
        sys.stdout.flush()
        title = venue["title"].encode('ascii', 'ignore')
        directory = "{}/venue_images/{}".format(rootDir, title)
        if not os.path.exists(directory):
            os.makedirs(directory)
        for i in range(len(venue["place"].photos)):
            photo = venue["place"].photos[i]
            photo.get(maxheight=2000)
            imageName = "{}_{}.{}".format(venue["title"], i, photo.mimetype[6:])
            imageLocation = "{}/{}".format(directory, imageName)
            imageFile = codecs.open(imageLocation, mode='w')
            imageFile.write(photo.data)
            imageFile.close()
    # generateImagePreview()
    # global venues
    # choice = 0
    # showTableForVenues()
    # while choice >= 0:
    #     choice = raw_input("Select a number to pick image or -1 to exit: ")
    #     try:
    #         num = int(choice)
    #     except ValueError:
    #         print("Invalid Choice")
    #         continue
    #
    #     if num == -1:
    #         break
    #
    #     if num >= len(venues) or num < 0:
    #         print("Invalid Choice")
    #         continue
    #
    #     addImageToVenueAt(num)
    #     showTableForVenues()


def reAddPlaces():
    global places, removedPlaces
    choice = 0

    while choice >= 0:
        showTableForRemoved()
        choice = raw_input("Select a number to reAdd (it will disappear from this list, but it will be back in the other list) or 'b' to go back: ")
        if choice == 'b':
            return
        try:
            num = int(choice)
        except ValueError:
            print("Invalid Choice")
            continue

        if num == -1:
            break

        if num >= len(places) or num < 0:
            print("Invalid Choice")
            continue

        places.append(removedPlaces[num])
        del removedPlaces[num]
    print("\n")

def removePlaces():
    global places, removedPlaces, skip
    removeDuplicatePlaces()
    removeUnwanted()
    choice = 0
    while choice >= 0:
        showTableForPlaces()
        choice = raw_input("Enter comma separated numbers to remove (ie '1,2,8'), 'r' to show removed places or 'e' to exit: ")
        if choice == 'r':
            reAddPlaces()
            continue
        elif choice == 'e':
            break

        indices = []
        try:
            indices = [int(x) for x in choice.split(',') if x]
            # num = int(choice)
        except ValueError:
            print("Invalid Choice")
            continue
        idsToRemove = []
        for index in indices:
            if index < len(places) and index >= 0:
                idsToRemove.append(places[index].place_id)
        filteredPlaces = []
        for place in places:
            if place.place_id in idsToRemove:
                removedPlaces.append(place)
            else:
                filteredPlaces.append(place)

        places = filteredPlaces
    for place in removedPlaces:
        if place.place_id not in skip:
            skip.append(place.place_id)
    print("\n")
        # if num >= len(places) or num < 0:
        #     print("Invalid Choice")
        #     continue

        # removedPlaces.append(places[num])
        # del places[num]
        # showTableForPlaces()

def parseHoursOfOperation(place):
    if "opening_hours" not in place.details:
        print(color.BOLD + "{} has no hours of operation".format(place.name) + color.END)
        return []
    periods = place.details["opening_hours"]["periods"]
    days = []
    for dayHours in periods:

        day = dayHours["open"]["day"]
        if "close" not in dayHours:
            dayHoursParsed = {
                                "day" : day,
                                "is_open" : 1,
                                "start_time" : "06:00:00",
                                "end_time": "06:00:00",
                                "is_next_day": 1
            }
            days.append(dayHoursParsed)
            continue

        dayClose = dayHours["close"]["day"]
        if day == 0:
            day = 7
        openTimePre = dayHours["open"]["time"]
        closeTimePre = dayHours["close"]["time"]
        openTime = "{}:{}:00".format(openTimePre[:2], openTimePre[2:])
        closeTime = "{}:{}:00".format(closeTimePre[:2], closeTimePre[2:])
        isNextDay = 0 if day == dayClose else 1

        dayHoursParsed = {
                            "day" : day,
                            "is_open" : 1,
                            "start_time" : openTime,
                            "end_time": closeTime,
                            "is_next_day": isNextDay
        }
        days.append(dayHoursParsed)

    return days


def addVenue(place):
    placeDetails = {
                    "title": place.name.encode('ascii', 'ignore'),
                    "lat": place.geo_location["lat"],
                    "lon": place.geo_location["lng"],
                    "address": place.vicinity.encode('ascii', 'ignore'),
                    }
    if place.website is not None:
        placeDetails["website_url"] = place.website
    if "formatted_phone_number" in place.details:
        placeDetails["phone"] = place.details["formatted_phone_number"]
    for component in place.details["address_components"]:
        if "neighborhood" in component["types"]:
            placeDetails["city"] = component["long_name"].encode('ascii', 'ignore')
        elif "locality" in component["types"] and "city" not in placeDetails:
            placeDetails["city"] = component["long_name"].encode('ascii', 'ignore')
        elif "administrative_area_level_1" in component["types"]:
            placeDetails["state"] = component["short_name"].encode('ascii', 'ignore')

    placeDetails["hours_of_operation"] = parseHoursOfOperation(place)
    placeDetails["place"] = place
    global venues
    venues.append(placeDetails)




def startIndividualScrape(lat, lon, radius, venue_type, pt):

    if pt == []:
        # query_result = google_places.nearby_search(
        #     lat_lng=dict(lat=lat, lng=lon), rankby='distance',
        #     radius=radius, types=[venue_type], keyword='nightlife')
        query_result = google_places.nearby_search(
            lat_lng=dict(lat=lat, lng=lon), rankby='distance',
            radius=radius, keyword=keyword)
    else:
        # query_result = google_places.nearby_search(
        #     lat_lng=dict(lat=lat, lng=lon), rankby='distance',
        #     radius=radius, types=[venue_type], keyword='nightlife', pagetoken=pt)
        query_result = google_places.nearby_search(
            lat_lng=dict(lat=lat, lng=lon), rankby='distance',
            radius=radius, keyword=keyword, pagetoken=pt)

    return query_result

def startScrape(coordinates, radius, types, startLocation):
    numLocations = len(coordinates)
    currentNum = 1
    failed = []
    for (lat, lon) in coordinates:
        if startLocation <= currentNum:
            print("Starting Scrape {} of {}".format(currentNum, numLocations))
            for venue_type in types:
                success = scrape(lat, lon, radius, venue_type, 1.5)
                if not success:
                    failed.append((lat,lon))

        else:
            print("Skipping Scrape {} of {}".format(currentNum, numLocations))
        currentNum += 1

    if len(failed) > 0:
        print("Retrying Failed Scrapes...")
    for (lat, lon) in failed:
        for venue_type in types:
            success = scrape(lat, lon, radius, venue_type, 3)
            if not success:
                print("Failed Again... Not Trying Again")


def handleResults(query_result):
    global places
    # for place in query_result.places:
    #     if place.name == "Bootsy Bellows":
    #         places.append(place)

    places += query_result.places


def degreesFor(n):
    degrees = []
    num = 1
    initialD = 30
    if n >= 0:
        num = 6 * n
        initialD = 30/n
    d = initialD
    i = 0
    while d < 360:
        d = initialD
        if i > 0:
            d = initialD + (60/n * i)
        if d < 360:
            degrees.append(d)
        i += 1
    return degrees


def coordinatesForLayer(lat, lon, radius, n):
    coordinates = []
    R = 2 * radius/69 * n
    degrees = degreesFor(n)
    for d in degrees:

        dRad = math.radians(d)
        x_offset = math.cos(dRad) * R
        y_offset = math.sin(dRad) * R
        x = lat + x_offset
        y = lon + y_offset
        coordinates.append((x,y))
    return coordinates

def coordinatesFor(lat, lon, radius, maxDistance):
    coordinates = [(lat, lon)]
    if radius != maxDistance:
        numLayers = numLayersFor(radius, maxDistance)
        for i in range(0, int(numLayers)):
            coordinates += coordinatesForLayer(lat, lon, radius, i + 1)
    coordString = ""
    for (lat1, lon1) in coordinates:
        if coordString != "":
            coordString += "\n"
        coordString += "{},{}".format(lat1,lon1)
    pyperclip.copy(coordString)
    print("\n\nCoordinates Copied To Clipboard\n\n")
    return coordinates

def numLayersFor(radius, maxDistance):
    return math.ceil(maxDistance/radius)

def uploadVenues(outputHandler):
    global skip
    duplicates = []
    for venue in venues:
        duplicate = outputHandler.checkIfPossibleDuplicate(venue)
        if duplicate is not None:
            duplicates.append((venue, duplicate))
        elif shouldUploadResults:
            outputHandler.uploadVenueToServer(venue)
        else:
            print("Did not upload results. Uploading was disabled.")


    while len(duplicates) > 0:
        tableData = [["num", "Name", "Address"]]
        for i in range(len(duplicates)):
            venue, duplicate = duplicates[i]
            title = "{}\n{}".format(venue["title"], duplicate["title"])
            address = "{}\n{}".format(venue["address"], duplicate["address"])
            data = [i, title, address]
            tableData.append(data)
        table = AsciiTable(tableData)
        print("\n")
        print(table.table)

        choice = raw_input("Select a number to upload anyways, press 'd' when done: ")
        if choice == 'd':
            break
        else:
            try:
                num = int(choice)
            except ValueError:
                print("Invalid Choice")
                continue
            if num < len(duplicates) and num >= 0:
                venue, duplicate = duplicates[num]
                if shouldUploadResults:
                    outputHandler.uploadVenueToServer(venue)
                else:
                    print("Did not upload results. Uploading was disabled.")
                del duplicates[num]
            else:
                print("Invalid Choice")
                continue
    for venue, duplicate in duplicates:
        place = venue["place"]
        skip.append(place.place_id)

def generateImagePreview():
    rowsHtml = ""
    f=codecs.open("ImagePreviewRowTemplate.html", encoding='utf-8')
    rowHtmlTemplate = f.read()
    for venue in venues:
        place = venue["place"]
        details = {
            "name" : place.name
        }
        for i in range(10):
            url = ""
            if i < len(place.photos):
                photo = place.photos[i]
                # photo.get(maxheight=2000)
                url = photo.url
            details["image{}".format(i)] = url
        rowHtml = rowHtmlTemplate.format(**details)
        rowsHtml += rowHtml

    f=codecs.open("ImagePreviewPage.html", encoding='utf-8')
    bodyTemplate = f.read()
    bodyDetails = {
                    "rows" : rowsHtml
    }
    bodyHtml = bodyTemplate.format(**bodyDetails)
    imagePreview = codecs.open("/temp/imagePreview.html", mode='w', encoding='utf-8')
    imagePreview.write(bodyHtml)
    imagePreview.close()
    webbrowser.open_new_tab("/temp/imagePreview.html")




if __name__ == "__main__":
    print(chr(27) + "[2J")
    potentialConfigs = []
    for file in os.listdir('.'):
        if fnmatch.fnmatch(file, 'GoogleScrapeDetails*.json'):
            potentialConfigs.append(file)

    if len(potentialConfigs) == 0:
        print("No Configuration File Found")
        sys.exit()
    elif len(potentialConfigs) > 1:
        choice = None
        while choice is None or choice < 0 or choice > len(potentialConfigs) - 1:
            for i in range(len(potentialConfigs)):
                print("{}: {}".format(i, potentialConfigs[i]))
            choice = int(raw_input("Select a config file [0]: ") or 0)

        configFile = potentialConfigs[choice]
    else:
        configFile = potentialConfigs[0]

    lastFile = open(configFile)
    lastString = lastFile.read()
    lastFile.close()
    details = json.loads(lastString)
    last = details["last"]
    skip = details["skip"]
    blacklist = details["blacklist"]
    mandatoryTypes = details["mandatoryTypes"]
    acceptedTypes = details["acceptedTypes"]
    shouldUploadResults = True

    filename = raw_input("Enter a filename to save as csv or leave blank to upload to server [None]: ") or None
    keyword = raw_input("Enter keyword to search for [nightlife]: ") or "nightlife"

    if not shouldUploadResults:
        print("UPLOADING IS DISABLED. RE-ENABLE BEFORE PERFORMING AN ACTUAL SCRAPE")

    lastLocation = "{},{}".format(last["lat"], last["lon"])
    newLocation = raw_input("Location 'lat, lon' [{}]: ".format(lastLocation)) or lastLocation
    if newLocation is None:
        lat = last["lat"]
        lon = last["lon"]
    else:
        locSplit = newLocation.split(",")
        lat = float(locSplit[0])
        lon = float(locSplit[1])



    # lat = input("Latitude: ")
    # lon = input("Longitude: ")
    # lat = 34.1867655 # office
    # lon = -118.59554230000003 # office
    # lat =  34.09085 # 1 oak
    # lon =  -118.38894 # 1 oak
    # locationName = raw_input("Location Name: ")
    outputHandler = OutputHandler(filename)
    c = []
    radius = 0
    choice = "r"
    while choice != "c":
        if choice == "r":
            radius = float(raw_input("Radius for each individual search [{}]: ".format(last["radius"])) or last["radius"])
            if radius is None:
                radius = last["radius"]
            maxDistance = float(raw_input("Total distance to search [{}]: ".format(last["maxDistance"])) or last["maxDistance"])
            if maxDistance is None:
                maxDistance = last["maxDistance"]
            # startLocation = int(raw_input("Start Location (enter to start at beginning): ") or "0")
            startLocation = 0
            c = coordinatesFor(lat, lon, radius, maxDistance)
        elif choice == "b":
            webbrowser.open_new_tab("https://www.darrinward.com/lat-long/")
        print("r = Rechoose Parameters, b = Open browser to Map Site, c = continue")
        choice = raw_input("Choice: ")

    last["lat"] = lat
    last["lon"] = lon
    last["radius"] = radius
    last["maxDistance"] = maxDistance
    lastFile = open(configFile, mode='w')
    details["last"] = last
    detailsJson = json.dumps(details)
    lastFile.write(detailsJson)
    lastFile.close()


    startScrape(c, radius, [types.TYPE_BAR], startLocation)
    # startScrape(c, radius, [types.TYPE_NIGHT_CLUB], startLocation)
    # showTableForPlaces()
    removePlaces()

    for place in places:
        place.get_details()
        addVenue(place)

    uploadVenues(outputHandler)

    lastFile = open(configFile, mode='w')
    details["skip"] = skip
    detailsJson = json.dumps(details)
    lastFile.write(detailsJson)
    lastFile.close()

    if filename is not None:
        outputHandler.file.close()

    print("Getting images...")
    addVenueImages()



        # print("{} | {}".format(place.name, place.vicinity))

    # startScrape(lat, lon, outputHandler)
