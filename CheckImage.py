#! /usr/bin/python

import requests, validators, os, MySQLdb, re, json, codecs, webbrowser, time, signal, sys, datetime
from pprint import pprint

db_creds = {
    "server" : "partiac.com",
    "user" : "scraper",
    "passwd" : "p-scraper-123",
    "db" : "partiac"
}
db = MySQLdb.connect(
                        db_creds["server"],
                        db_creds["user"],
                        db_creds["passwd"],
                        db_creds["db"])

def getImageURLs():
    sql = "SELECT id, venue_profile_pic FROM users WHERE role_id = 2 AND venue_profile_pic <> ''"
    cursor = db.cursor()
    try:
        cursor.execute(sql)
    except Exception as e:
        print(e)

    imageResult = cursor.fetchall()
    imageDic = {}
    for result in imageResult:
        venue = result[0]
        imageName = result[1]
        imageURL = "http://partiac.com/image.php?image=uploads/users/{}&width=250&height=250".format(imageName)
        imageDic[venue] = imageURL
    return imageDic

def checkURL(url):
    session = requests.Session()
    image = session.get(url)

    return image.content is not None and len(image.content) > 1000 and image.content != checkImage and image.content != checkNoImage

def clearProfilePics(venues):
    venuesList = ", ".join(venues)
    sql = "UPDATE users SET venue_profile_pic = '' WHERE id IN ({})".format(venuesList)
    cursor = db.cursor()
    try:
        cursor.execute(sql)
    except Exception as e:
        print(e)
    db.commit()

def removeFromSkip(venues):
    skipFile = open('skip.json')
    skipString = skipFile.read()
    skipFile.close()
    skip = json.loads(skipString)["skip"]
    for i in range(len(skip)):
        if skip[i] in venues:
            del skip[i]
    skipFile = open('skip.json', mode='w')
    skipJson = json.dumps({"skip": skip})
    skipFile.write(skipJson)
    skipFile.close()


if __name__ == "__main__":
    session = requests.Session()
    checkImage = session.get("http://partiac.com/image.php?image=uploads/users/Studio200.jpg&width=250&height=250").content
    checkNoImage = session.get("http://partiac.com/image.php?image=uploads/users/The-Bubbler-149099183944.jpg&width=250&height=250").content
    imageDic = getImageURLs()
    failedVenues = []
    count = 0
    for venue, url in imageDic.iteritems():
        count = count + 1
        print "{}/{} Checking venue: {}".format(count, len(imageDic), venue)
        if not checkURL(url):
            print "Failed"
            failedVenues.append(str(venue))

    clearProfilePics(failedVenues)
    removeFromSkip(failedVenues)
