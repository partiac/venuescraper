#! /usr/bin/python

import requests, validators, os, MySQLdb, re, json, codecs, unicodedata, webbrowser
from pprint import pprint
from bs4 import BeautifulSoup
from urlparse import urlparse, parse_qsl
from paramiko import SSHClient, WarningPolicy
from scp import SCPClient
from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.proxy import *
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


db_creds = {
    "server" : "partiac.com",
    "user" : "scraper",
    "passwd" : "p-scraper-123",
    "db" : "partiac"
}
db = MySQLdb.connect(
                        db_creds["server"],
                        db_creds["user"],
                        db_creds["passwd"],
                        db_creds["db"])


def getChromeDriver():
    chrome_options = Options()
    chrome_options.add_experimental_option("prefs", {'profile.managed_default_content_settings.images': 2})
    driverPath = "./chromedriver-mac"
    driver = webdriver.Chrome(chrome_options = chrome_options, executable_path=driverPath)
    return driver

def getProfilePicture(fbId, soup):
    # htmlDoc = codecs.open('/tmp/doc.html', mode='w', encoding='utf-8')
    # htmlDoc.write(soup.prettify())
    # htmlDoc.close()
    #
    url = None
    for tag in soup.find_all('a'):
        if tag.get('aria-label') == "Profile picture":
            for img in tag.find_all('img'):
                url = img.get("src")
                break
            break
    if url is not None:
        # url = "https://graph.facebook.com/{}/picture?width=2000&height=2000".format(fbId)
        session = requests.Session()
        profPic = session.get(url)
        return profPic.content


def getAboutPage(fbId):
    url = "https://www.facebook.com/pg/{}/about/".format(fbId)
    # session = requests.Session()
    # page = session.get(url)
    browser.get(url)
    html = browser.page_source
    # saveFile = codecs.open('html_dumps/{}.html'.format(fbId), mode='w', encoding='utf-8')
    # saveFile.write(page.text)
    # saveFile.close()
    soup = BeautifulSoup(html, "html.parser")
    return soup

def scrapeAbout(soup):
    for tag in soup.find_all(class_="_50f4"):
        if tag.string == "About":
            nextTag = tag.next_sibling
            return nextTag.getText()

def scrapeSocialMedia(soup, needsTwitter, needsInstagram, needsWebsite, needsEmail):
    contactLocations = {}
    for tag in soup.find_all(class_="_50f7"):
        if tag.string == "ADDITIONAL CONTACT INFO":
            for section in tag.parent.parent.find_all(class_="_50f4"):
                text = section.getText()
                if validators.email(text) and needsEmail:
                    contactLocations["email"] = text
                elif validators.url(text) and needsWebsite:
                    contactLocations["website_url"] = text
                else:
                    href = section.parent.get("href")
                    if href is not None:
                        if "instagram" in href and needsInstagram:
                            if text[:1] == "@":
                                text = text[1:]
                            contactLocations["instagram_url"] = text
                        elif "twitter" in href and needsTwitter:
                            if text[:1] != "@":
                                text = "@" + text
                            contactLocations["twitter_url"] = text
                    # else:
                    #     contactLocations["twitter_url"] = text
    return contactLocations

def filterCategories(categories):
    categoryAliases = {
                        "Bar": [
                            "Bar",
                            "Gastropub",
                            "Cocktail Bar",
                            "Whisky Bar",
                            "Sports Bar",
                            "Pub",
                            "Dive Bar",
                            "Bar & Grill",
                            "Beer Bar"
                        ],

                        "Nightclub" : [
                            "Dance & Night Club",
                            "Performance & Event Venue",
                            "Party Entertainment Service"
                        ],

                        "Lounge": [
                            "Lounge",
                            "Brewery",
                        ],

                        "LGBTQ": [
                            "Gay Bar"
                        ],

                        "Hookah": [
                            "Hookah Lounge"
                        ]
    }


    filteredCategories = []
    for unfilteredCat in categories:
        for category, aliases in categoryAliases.iteritems():
            if unfilteredCat in aliases and category not in filteredCategories:
                filteredCategories.append(category)
    return filteredCategories


def getMenuAndCategories(soup, needsMenu, needsCategories):
    details = {}
    categories = []
    for tag in soup.find_all("a"):
        href = tag.get("href")
        if href is not None:
            if tag.string == "See Menu":
                menuUrl = resolveURL(href)
                if menuUrl is not None and needsMenu:
                    details["menu_url"] = menuUrl
                # else:
                #     print "Error resolving menu url: {}".format(href)

            elif "/search/str/" in href and "/keywords_pages/?ref=page_about_category" in href and needsCategories:
                categories.append(tag.string)

    if len(categories) > 0:
        filterCategoriesData = filterCategories(categories)
        if len(filterCategoriesData) > 0:
            if "LGBTQ" in filterCategoriesData:
                filterCategoriesData = ["LGBTQ"]
            details["filter_atmosphere"] = ",".join(filterCategoriesData)
        # else:
        #     pprint(categories)
    # else:
        # print "No Categories Found"

    return details

def resolveURL(url):
    o = urlparse(url)
    q = parse_qsl(o.query)
    for var, url in q:
        if var == "u":
            return url

def uploadProfilePic(pic, imageName):
    imageLoc = "/tmp/{}.jpg".format(imageName)
    with open(imageLoc, 'wb') as outf:
        outf.write(pic)

    from paramiko import SSHClient
    from scp import SCPClient

    ssh = SSHClient()
    ssh.set_missing_host_key_policy(WarningPolicy)
    # ssh.load_system_host_keys()
    ssh.connect('partiac.com', username="ubuntu", key_filename=".private_ssh/partiac_key.pem")

    scp = SCPClient(ssh.get_transport())
    scp.put(imageLoc, remote_path="/var/www/html/app/webroot/uploads/users/")
    scp.close()
    os.remove(imageLoc)
    return "{}.jpg".format(imageName)

def scrape(venue):
    detailsDic = {}
    fbId = venue["fbId"]
    soup = getAboutPage(fbId)
    if venue["needsAbout"]:
        about = scrapeAbout(soup)
        if about is not None:
            detailsDic["about"] = about
    if venue["needsTwitter"] or venue["needsInstagram"] or venue["needsEmail"] or venue["needsWebsite"]:
        detailsDic.update(scrapeSocialMedia(soup, venue["needsTwitter"], venue["needsInstagram"], venue["needsWebsite"], venue["needsInstagram"]))

    if venue["needsMenu"] or venue["needsCategories"]:
        detailsDic.update(getMenuAndCategories(soup, venue["needsMenu"], venue["needsCategories"]))
    # profilePicUrl = getProfilePicURL(soup)
    # print profilePicUrl
    if venue["needsImage"]:
        profilePic = getProfilePicture(fbId, soup)
        if profilePic is not None:
            print "Uploading Image"
            profilePicImage = uploadProfilePic(profilePic, fbId)
            detailsDic["venue_profile_pic"] = profilePicImage


    # pprint(detailsDic)


    return detailsDic

def getVenues(skip):
    venues = []
    # sql = "SELECT id, facebook_url, venue_profile_pic, about, twitter_url, instagram_url, website_url, email, menu_url, filter_atmosphere FROM users WHERE role_id = 2 AND id = 9462"
    sql = "SELECT id, facebook_url, venue_profile_pic, about, twitter_url, instagram_url, website_url, email, menu_url, filter_atmosphere FROM users WHERE role_id = 2 AND facebook_url <> '' AND facebook_url NOT LIKE '%pages%' AND (about = '' OR twitter_url = '' OR instagram_url = '' OR website_url = '' OR email = '' OR menu_url = '' OR filter_atmosphere = '' OR venue_profile_pic = '') ORDER BY created DESC"

    cursor = db.cursor()
    try:
        cursor.execute(sql)
    except Exception as e:
        print(e)
        return venues

    venuesResult = cursor.fetchall()
    for venueDetails in venuesResult:
        venueId = venueDetails[0]
        if venueId in skip:
            continue
        fbUrl = venueDetails[1]
        needsImage = venueDetails[2] is None or venueDetails[2] == ''
        needsAbout = venueDetails[3] is None or venueDetails[3] == ''
        needsTwitter = venueDetails[4] is None or venueDetails[4] == ''
        needsInstagram = venueDetails[5] is None or venueDetails[5] == ''
        needsWebsite = venueDetails[6] is None or venueDetails[6] == ''
        needsEmail = venueDetails[7] is None or venueDetails[7] == ''
        needsMenu = venueDetails[8] is None or venueDetails[8] == ''
        needsCategories = venueDetails[9] is None or venueDetails[9] == ''
        fbIdMatch = re.search(".*facebook.com\/(pg/)*([a-zA-Z0-9-_\.]*)/?", fbUrl)
        if fbIdMatch is not None:
            fbId = fbIdMatch.group(2)
            venue = {
                        "id": venueId,
                        "fbId" : fbId,
                        "needsImage": needsImage,
                        "needsAbout": needsAbout,
                        "needsTwitter": needsTwitter,
                        "needsInstagram": needsInstagram,
                        "needsWebsite": needsWebsite,
                        "needsEmail": needsEmail,
                        "needsMenu": needsMenu,
                        "needsCategories": needsCategories

            }
            venues.append(venue)
    return venues

def uploadVenues(venueDetails):
    for venueId in venueDetails:
        print "Uploading details for: {}".format(venueId)
        setStrings = []
        for key, val in venueDetails[venueId].iteritems():
            val = val.encode('ascii', 'ignore')
            val = MySQLdb.escape_string(val)
            setStrings.append("{} = \"{}\"".format(key, val))
        setString = ",".join(setStrings)

        sql = "UPDATE users SET {} WHERE id = {}".format(setString, venueId)

    #     cursor = db.cursor()
    #     try:
    #         cursor.execute(sql)
    #     except Exception as e:
    #         print(e)
    #
    # db.commit()

def uploadVenue(venueId, venue):

    print "Uploading details for: {}".format(venueId)
    setStrings = []
    for key, val in venue.iteritems():
        val = val.encode('ascii', 'ignore')
        val = MySQLdb.escape_string(val)
        setStrings.append("{} = \"{}\"".format(key, val))
    setString = ",".join(setStrings)

    sql = "UPDATE users SET {} WHERE id = {}".format(setString, venueId)

    cursor = db.cursor()
    try:
        cursor.execute(sql)
    except Exception as e:
        print(e)

    db.commit()

def setFBCookies():
    global cookies
    cookieFile = codecs.open('cookies.json')
    cookies = json.loads(cookieFile.read())
    cookieFile.close()
    browser.get("http://facebook.com")
    browser.delete_all_cookies()
    for name, value in cookies.iteritems():
        browser.add_cookie({'name': name, 'value': value})

    while True:
        browser.get("http://facebook.com")
        choice = raw_input("Make sure facebook is logged in, type 'd' if it is, type 'r' if it isn't: ")
        if choice == 'r':
            browser.delete_all_cookies()
            browser.get("http://facebook.com")
            raw_input("Login to facebook and hit Enter when you are done.")
        elif choice == 'd':
            break


    c_user = browser.get_cookie('c_user')
    xs = browser.get_cookie('xs')
    cookies = {'c_user': c_user['value'], 'xs': xs['value']}
    cookieJSON = json.dumps(cookies)
    cookieFile = codecs.open('cookies.json', 'w')
    cookieFile.write(cookieJSON)
    cookieFile.close()

if __name__ == "__main__":
    skipFile = open('skip.json')
    skipString = skipFile.read()
    skipFile.close()
    skip = json.loads(skipString)["skip"]
    venues = getVenues(skip)
    venuesDetails = {}
    browser = getChromeDriver()
    setFBCookies()

    print "Scraping {} venues".format(len(venues))
    count = 0
    for venue in venues:
        count = count + 1
        print "\n{}/{} Scraping for: {}".format(count, len(venues), venue["id"])
        details = scrape(venue)
        if len(details) > 0:
            uploadVenue(venue["id"], details)

        skip.append(venue["id"])
        skipFile = open('skip.json', mode='w')
        skipJson = json.dumps({"skip": skip})
        skipFile.write(skipJson)
        skipFile.close()
            # venuesDetails[venue["id"]] = details
    # pprint(venuesDetails)
    # uploadVenues(venuesDetails)

    browser.quit()
